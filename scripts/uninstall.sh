#!/usr/bin/env sh

set -eu

( # using a subshell to avoid having to cd back
  cd "$(dirname "$0")/../"

# shellcheck disable=SC2046
  eval $(opam config env)

  make uninstall
)
