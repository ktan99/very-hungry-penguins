(** Utils module *)

(** print argument in the console *)
let _log s =
  Printf.eprintf "%s\n%!" s;
