
module type S = sig
  val grid : Move.elt Move.grid 
end

module Make (M : S) = struct
  let priority_size = ref 1000
  let prior_calls =ref 0

  module FinPos : Bitset.FIN with type t = Move.pos = struct
    type t = Move.pos
    let line_length = Array.length M.grid.(0)
    let max = (Array.length M.grid)*line_length - 1 
    let to_int pos = fst pos* line_length + snd pos
    let of_int n = (n / line_length, n mod line_length)
    (*les deux dernières fonction définissent une bijection des entiers vers les positions*)
  end
  (*Ce module sert à gérer des ensembles de positions via des bitsets*)

  module HSet : Bitset.SET with type elt = Move.pos = Bitset.Make(FinPos)

  let grid_set  = HSet.init (fun pos ->
      match M.grid.(fst pos).(snd pos) with
      |Move.ICE _ |Move.NOSTOP | Move.TELEPORT _ -> true
      |_-> false)
  (*crée l'ensemble de toutes les cases de glace *)

  let grid_of_set set = 
    let new_grid = Array.make_matrix (Array.length M.grid)
        (Array.length M.grid.(0))
        Move.WATER in
    (*crée une grille pleine d'eau*)
    for i = 0 to Array.length M.grid - 1 do
      for j = 0 to Array.length M.grid.(0) - 1 do 
        if HSet.member set (i,j) then 
          new_grid.(i).(j) <- Move.ICE 1
      done
    done;
    (*remplace les cases d'eau de l'ensemble par de la glace*)
    new_grid
  (* cette fonction crée une grille à partir d'un ensemble*)
  (* Cette grille ne contient que de la glace à 1 poisson et les manchots*)



  (*renvoie tous les mouvements autorisés*)
  let all_moves gs set elt =
    	
    let moves  = ref [] in 
    let rec explore_dir dir pos i =

	  
	  if HSet.member set pos then (*elt est-il dans set?*)
	    begin
	      if gs#get_cell (fst pos) (snd pos) <> Move.NOSTOP then
		moves := (dir,i)::!moves;
	      (* ici on utilise move_n et non move car move ne prend pas en compte 
	       * la téléportation*)
              explore_dir dir (Move.move_n (gs#get_map ()) pos (dir,1)) (i+1)
	    (* si oui:on retient le chemin, on se déplace d'encore un cran
	     * et on recommence*)
	    end;

    in 
    List.iter (fun dir -> explore_dir dir (Move.move_n (gs#get_map ())
						       elt (dir,1)) 1)
	      Move.all_directions;
    (* applique cette fonction auxiliaire à toutes les directions*)
    !moves




  let  accessible gs set elt = 
    (*ajoute l'ensemble des élément accessibles depuis elt à set*)
    let rec explore el dirs cc = match dirs with 
      |[] -> cc
      |dir::q ->

	let (is_legal,next_el) =
	  Move.special_legal_move_n (gs#get_map ()) el (dir,1) in
		(*if (fst el) = 3 then (
	  Printf.printf "(%d,%d) \n" (fst el) (snd el);
	  if is_legal && dir = Move.E && not (HSet.member cc next_el) &&  HSet.member set next_el then 
	    Printf.printf "blub : (%d,%d)" (fst next_el) (snd next_el)
	);*)
        if is_legal && HSet.member set next_el && not (HSet.member cc next_el) then
            (* si le nouvel élément est dans set sans être déjà vu (dans cc) *)
            explore el q (explore
                            next_el
                            Move.all_directions
                            (HSet.add cc next_el))
          (*on explore depuis le nouveau points dans toutes les directions
           * et on ajoute le point dans la liste des points déjà vus*)
        else
          explore el q cc (*sinon, on continue*)
    in
    explore elt Move.all_directions (HSet.add HSet.empty elt)



  let neighbours gs set elt =
    (*calcule l'ensemble des voisins*)
    let nb = Array.make 6 (-1,-1) in (*tableau où l'on stockera les voisins*)
    let rec elim_ice l i = match l with 
      |[] -> ()
      |dir::q -> 
        let next_el = Move.move_n (gs#get_map ()) elt (dir,1) in 
        (*on se déplace d'un cran dans une direction *)
        if HSet.member set next_el then
          nb.(i) <- next_el;
        elim_ice q (i+1)
        (*si on obtient un élément de l'ensemble, c'est un voisin valide*)
        (*sinon, on continue à chercher*)
    in
    elim_ice Move.all_directions 0;
    nb


  let disconnected gs set elt =
    (* on transfomre le tableau de voisins renvoyé par la fonction 
     * précédente en ensemble*)
    let nbours = neighbours gs set elt in
    let set_neighbours = ref HSet.empty in 
    let a_neighbour = ref (-1,-1) in (*un des voisins*)
    for i = 0 to Array.length nbours - 1 do 
      if fst nbours.(i) >= 0 then (
        set_neighbours := HSet.add !set_neighbours nbours.(i);
        a_neighbour := nbours.(i)	  
      )
    done;
    (* ici, l'idée est de regarder la connexité de l'ensemble formé 
     * uniquement des voisins de [el]*)
    if fst !a_neighbour >= 0 then
      (*si il y a au moins 1 voisin*)
      HSet.cardinal (accessible gs !set_neighbours !a_neighbour) <>
      HSet.cardinal !set_neighbours
    (* on vérifie qu'il puisse accéder à d'autre éléments que ses 
     * voisins directs*)
    else
      false




  let split gs set elt =
    (* sépare en deux composantes connexes au point elt*)
    let ccs = ref [] in (*ccs pour composantes connexes*)
    let nbours = neighbours gs set elt in
    let del_elt = HSet.remove set elt in 

    for i = 0 to Array.length nbours - 1 do
      if fst nbours.(i) >= 0 then
        begin
          let new_cc = accessible gs del_elt nbours.(i) in
          for j = i + 1 to Array.length nbours - 1 do
            if fst nbours.(i) >= 0 && HSet.member new_cc nbours.(j) then
              nbours.(j) <- (-1,-1)
          done;
          ccs := new_cc :: !ccs
        end
        (* concrètement, on considère un voisin, et on regarde sa composante 
	 * connexe si on supprime l'élément elt.*)
    done;
    !ccs

  (*Cette fonction renvoie Some [la même chose que accessible] si
   * la composante connexe accessible ne contient pas de pengouin (excepté
   * le pengouin situé en penguin pos) 
   * Il est recommandé de ne pas l'utiliser (et donc de ne pas la mettre)
   * dnans le mli. Utiliser à la place split_exclusive*)   
  let accessible_exclusive gs set elt penguin_pos =
    let rec explore el dirs cc =
      match dirs with
      |[] -> Some cc
      |dir::q-> 
        let next_el =  Move.move_n (gs#get_map ()) el (dir,1) in
        if HSet.member set next_el then
          if  not (HSet.member cc next_el) then
            begin
              match explore next_el
                      Move.all_directions
                      (HSet.add cc next_el) with
              |None -> None
              |Some x ->explore el q x
            end
          else
            explore el q cc
        else
          begin
            if gs#get_cell (fst next_el) (snd next_el) = Move.PENGUIN
            && next_el <> penguin_pos then
              None
            else
              explore el q cc
          end
    in
    explore elt Move.all_directions (HSet.add HSet.empty elt)



  let split_exclusive gs set elt =
    let ccs = ref [] in (*ccs pour composantes connexes*)
    let nbours = neighbours gs set elt in
    let del_elt = HSet.remove set elt in 

    for i = 0 to Array.length nbours - 1 do
      if fst nbours.(i) >= 0 then
        begin
          match accessible_exclusive gs del_elt nbours.(i) elt with
          |None -> ()
          |Some new_cc ->
            for j = i + 1 to Array.length nbours - 1 do
              if fst nbours.(i) >= 0 && HSet.member new_cc nbours.(j) then
                nbours.(j) <- (-1,-1)
            done;
            ccs := new_cc :: !ccs
        end
    done;
    !ccs
  
  (*à chaque configuration, on associe un nombre de marquage, 
   * un pré-chemin, sa longueur,  un post-chemin et sa longueur
   * (tout ça dans status)*)
  type configuration = int * (int * Move.move list) * (int * Move.move list)

  module HMap = struct 
    let table = Hashtbl.create 100



    let rm_conf conf =
      Hashtbl.remove table conf
    (*supprimer la configuration de hash conf de la table de hash table*)

    let add_conf conf status =
      rm_conf conf; (*ceci permet de garder une taille raisonnable de la table*)
      Hashtbl.add table conf status 
    (*ajoute une configuration dans la table de hash*)
    (* XXX Hashtbl.replace aurait été avantageux *)
    let find_conf conf = 
      try 
        Some (Hashtbl.find table conf)
      with 
        Not_found -> None
      (*trouve la configuration de hash conf, renvoie None si il n'y en a pas*)

  end

  (*le premier entier  est la longueur du pré chemin, le second le nombre de 
   * cases restantes le booléen vaut le résultat de l'heuristique disconnected 
   *)
  module KeysDis : Priority.ORDERED with type t = bool*int*int = struct 
    type t = bool*int*int
    let compare (b1,x1,x2) (b2,y1,y2) =
      match (b1,b2) with
      |(false,true) -> -1
      |(true,false) -> 1
      |_ ->
        let sum1 = 2*x1 + x2 and sum2 = 2*y1 +y2 in
        if sum1 > sum2 then 
          -1
        else if sum1 = sum2 then 
          0
        else
          1
  end

  module KeysSum : Priority.ORDERED with type t = bool*int*int = struct 
    type t = bool*int*int
    let compare (_,x1,x2) (_,y1,y2) =
      let sum1 = 2*x1 + x2 and sum2 = 2*y1 +y2 in
      if sum1 > sum2 then 
        -1
      else if sum1 = sum2 then 
        0
      else
        1
  end

  module NoKeys : Priority.ORDERED with type t = bool*int*int= struct 
    type t =  bool*int*int
    let compare _ _ =
      0
  end


  module Prior = Priority.Make(KeysDis)



  (* permet de trouver la composante connexe à laquelle
   * appartient elt dans un tableau de composantes connexes ccs
   * Attention ! ccs doit être non vide et au moins une composante
   * doit contenir elt
  *)
  let rec find_cc ccs elt  =
    match ccs with
    |[] -> failwith "Aucune composante connexe n'a été trouvée"
    |cc::q -> if HSet.member cc elt then
        cc
      else
        find_cc q elt


  (*Renvoie le nombre de poissopns à la position pos*)
  let get_nb_fish gs pos =
    match gs#get_cell (fst pos) (snd pos) with
    |Move.ICE n -> n
    |_ -> 0


  (*The number of fishs in the set*)
  let fishs_on_set gs s =
    let sum = ref 0 in 
    HSet.iter s (fun el ->
        match gs#get_cell (fst el) (snd el) with
        |Move.ICE n -> sum := !sum + n
        |_ -> ()
      );
    !sum


  let max_path gs init_elt =
    (*initialisation*)
    let prior = Prior.create !priority_size (false,0,0) (HSet.empty,init_elt) in
    let acc = accessible gs grid_set init_elt in 
    (*Insertion de la première configuration dans la file de priorité*)
    ignore (Prior.insert prior
              (false,0,fishs_on_set gs grid_set)
              (grid_set,init_elt));
    let first_config = (-1,(0,(grid_set,init_elt)),(-1,[])) in
    HMap.add_conf (grid_set,init_elt) first_config;
    let max_path = ref (-1) in
    let root_tagged = ref false in

     
    (*si on est sur une feuille*)
    let rec lift_post_path conf (post_path_size,post_path) =
      match HMap.find_conf conf with
      |Some(tag_nb,(pre_path_size,pre_conf),(old_path_size,_)) ->
        if post_path_size > old_path_size then
	  begin
            let new_config =  (tag_nb,(pre_path_size,pre_conf),
			      (post_path_size,post_path)) in 
            HMap.add_conf conf new_config;
            (*il ne faut pas continuer si on est à le racine*)
            if pre_path_size > 0 then
              lift_post_path pre_conf (post_path_size + get_nb_fish gs (snd conf),
                                       (snd conf)::post_path)
            else (*sinon, mise à jour de max_path*)
              max_path := post_path_size

	  end
      |None -> failwith "lift_post_path : conf non trouvée"

    in

    let rec lift_tag conf =
      match HMap.find_conf conf with
      |Some(tag_nb,(pre_path_size,pre_conf),post_path) ->
        if fst post_path = 1 && pre_path_size > 0 then
          (*alors c'est une feuille et ce n'est pas la racine*)
          lift_tag pre_conf
        else
          begin
            HMap.add_conf conf (tag_nb - 1,(pre_path_size,pre_conf),post_path);

            if tag_nb = 1 then(
              if pre_path_size = 0 then (*on a gagné !*)
                root_tagged := true
              else
                lift_tag pre_conf
            )
          end
      |None -> failwith "Impossible de diminuer le tag d'une configuration"

    in 


    (* parcours des fils d'un noeud,
     * new_path_size vaut la taille de pre_path + 1 *)
    let rec browse_moves mov_l pre_conf ccs pre_path new_path_size =

      let add_pre_tag = ref 0 in

      let (_,elt) = pre_conf in 
      match mov_l with
      |[] -> 0
      |m::q ->
        let next_elt = Move.move_n (gs#get_map ()) elt m in 
        let next_set = find_cc ccs next_elt in
        if (new_path_size + fishs_on_set gs next_set <= !max_path) then
          lift_tag pre_conf
        else
          begin
            let next_conf = (next_set,next_elt) in

            match HMap.find_conf next_conf  with
            |None ->
              add_pre_tag := 1;
	      
              HMap.add_conf next_conf (-1,(new_path_size,pre_conf),(0,[]));
              ignore (Prior.insert prior
                        (disconnected gs next_set next_elt,
                         new_path_size,fishs_on_set gs next_set)
                        next_conf)
            |Some(nb_tag,(old_path_size,old_pre_conf),
                  (post_path_size,post_path)) ->
              if new_path_size > old_path_size && (nb_tag>0 || nb_tag= -1) then
                begin
                  add_pre_tag := 1;
                  lift_tag old_pre_conf;
		  (*gs#pp_grid Format.std_formatter (grid_of_set (fst next_conf));*)
                  HMap.add_conf next_conf (nb_tag,(new_path_size,pre_conf),
                                           (post_path_size,post_path));
                  (*TODO decrease_key*)
                  ignore (Prior.insert
                            prior
                            (disconnected gs next_set next_elt,
                             new_path_size,HSet.cardinal (fst pre_conf))
                            next_conf)
                end;
              lift_post_path pre_conf (post_path_size + get_nb_fish gs next_elt,
                                       next_elt::post_path)
          end;

        !add_pre_tag + browse_moves q pre_conf ccs  pre_path new_path_size
    in


    (*traitement d'une configuration*)
    let treat_conf conf =
     
      let (set,elt) = conf in
      match HMap.find_conf conf with
      (*si la configuration est dans la file normalement elle est dans la table
        de plus, elle ne doit pas être marquée*)
      |Some(tag_nb,(pre_path_size,pre_conf),_)
        when tag_nb <> 0 ->
	
        let moves = all_moves gs set elt in

        if moves = [] then
          (
	    
            lift_post_path conf (1,[]);
            lift_tag conf;
            HMap.add_conf conf (1,(pre_path_size,pre_conf),(0,[]))	    
          )
        else
          begin
	    
            let ccs = if disconnected gs set elt then
                split gs set elt
              else
                [HSet.remove set elt] in 
            let new_tag_nb = browse_moves moves conf ccs pre_conf
                (pre_path_size + 1) in
            if tag_nb = -1 then (
              (match HMap.find_conf conf with
               |Some(_,(pre_size,pre_conf),post) ->
                 HMap.add_conf conf (new_tag_nb,(pre_size,pre_conf),post);
                 if new_tag_nb = 0 then
                   lift_tag pre_conf		 
               |None -> failwith "conf non trouvée dans treat_conf");

            )

          end

      |_-> gs#pp_grid Format.std_formatter (grid_of_set set);
	failwith "Une configuration dans la file de priorité \
                     		     n'a pas été insérée dans la table"

    in


    (*boucle principale : gestion des priorités*)
    let rec max_path_tmp () =
      prior_calls := !prior_calls +1;
      if Prior.size prior > 0  && not !root_tagged then (
        treat_conf (Prior.value (Prior.extract_min prior));
        max_path_tmp ()
      )
    in


    
    max_path_tmp ();
     
    match HMap.find_conf (grid_set,init_elt) with
    |Some(_,_,(post_path_size,post_path)) ->(post_path_size - 1,post_path)
    |None -> failwith "L'algo a lamentablement échoué."





end

